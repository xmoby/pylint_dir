#!/usr/bin/env python2
"""
Runs pylint on a specified folder, recursively.
A summary is created for errors, and one report per .py file for the detailed messages.
"""

from __future__ import print_function

import argparse
import os
import re
import shutil
import subprocess
import sys

PYLINT_EXEC = 'pylint'
REPORT_DIR = ''
REPORT_DETAILED_DIR = ''
REPORT_SUMMARY_FILENAME = ''

class ModuleScore(object):
    """ Encapsulates the pylint score for a given module/file. """
    def __init__(self, filename='', score=0.0):
        """ Constructor, from values. """
        self.filename = filename
        self.score = score

class ReportMessage(object):
    """ Encapsulates a single pylint message, to put in the report. """
    def __init__(self, filename='', line=0, column=0, category='', message=''):
        """ Default constructor. """
        self.filename = filename
        self.line = line
        self.column = column
        self.category = category
        self.message = message.strip()

    @staticmethod
    def create_from_line(line, filename):
        """ Returns a ReportMessage object from the line if it's valid, or None if not. """
        if (line.startswith('E:') or line.startswith('W:') or
                line.startswith('R:') or line.startswith('C:')):
            split_by_colon = line.split(':')
            split_pos = split_by_colon[1].split(',')
            return ReportMessage(
                filename=filename,
                line=int(split_pos[0]),
                column=int(split_pos[1]),
                category=split_by_colon[0],
                message=split_by_colon[2])

        return None

class AnalysisResults(object):
    """ Encapsulates the analysis report. """
    def __init__(self, options):
        """ Default constructor. """
        self.options = options
        self.messages = []
        self.scores = []

    def analyze_dir(self):
        """ Analyzes the specified folder, recursively. """
        # Remove the previous report.
        if os.path.isdir(REPORT_DIR):
            empty_folder(REPORT_DIR)
        else:
            os.mkdir(REPORT_DIR)

        # Check all the files.
        for root, _, files in os.walk(self.options.dir):
            for name in files:
                filepath = os.path.join(root, name)
                self.__analyze_file(filepath)

    def __analyze_file(self, filename):
        """ Analyzes the specified file, and outputs the requested data where it belongs. """
        # First, make sure we are dealing with a .py file.
        if not filename.endswith('.py'):
            return False

        # Analyze that file.
        print('Analyzing {}...'.format(filename))
        pout = subprocess.Popen(
            '{} '
            '--unsafe-load-any-extension=y '
            '--ignored-classes=LookupDict '
            '{}'.format(PYLINT_EXEC, filename),
            shell=True, stdout=subprocess.PIPE, stderr=open(os.devnull, 'wb')).stdout.read()

        filename_for_report = os.path.relpath(filename, self.options.dir)

        # Prepare the details file.
        # FIXME: Should be optional (and probably OFF by default).
        details_filename = '{}.txt'.format(os.path.join(
            REPORT_DETAILED_DIR, os.path.relpath(filename, self.options.dir)))
        details_dirname = os.path.dirname(details_filename)

        if not os.path.isdir(details_dirname):
            os.makedirs(details_dirname)

        details_file = open(details_filename, 'w')

        for line in pout.decode('utf8').split('\n'):
            # Print all output in a dedicated file for the given .py file.
            details_file.write(line)

            # Save the message.
            self.__insert_message(filename_for_report, line)

            # Note the score.
            if 'Your code has been rated at' in line:
                score = float(re.findall(r"(-?\d+\.\d+)", line)[0])
                self.__insert_module_score(filename_for_report, score)

        return True

    def __insert_module_score(self, filename, score):
        """ Inserts a module score, sorted. """
        for i in range(len(self.scores)):
            if score < self.scores[i].score:
                self.scores.insert(i, ModuleScore(filename, score))
                return

        self.scores.append(ModuleScore(filename, score))

    def __insert_message(self, filename, line):
        """ Inserts a message from the given line, if the line represents a message. """
        message = ReportMessage.create_from_line(line, filename)

        if message:
            self.messages.append(message)

class AnalysisReport(object):
    """ Base class for report output. """
    def __init__(self, results):
        self.results = results

    def report(self):
        """ Writes the report. """
        # To be filled be extended class.
        return

class BasicReport(AnalysisReport):
    """ Exports the report in a summary file. """
    def report(self):
        """ Writes the summary file, after all has been analyzed. """
        summary_file = open(REPORT_SUMMARY_FILENAME, 'w')
        nb_files = len(self.results.scores)

        # Write the messages.
        summary_file.write('# Messages\n')
        # FIXME: Allow to filter out some messages/categories.
        nb_errors = 0
        nb_warnings = 0
        nb_conventions = 0
        nb_refactors = 0
        text_errors = ''
        text_warnings = ''
        text_conventions = ''
        text_refactors = ''

        for message in self.results.messages:
            if message.category == 'E':
                nb_errors += 1
                text_errors += self.__format_message(message)
            elif message.category == 'W':
                nb_warnings += 1
                text_warnings += self.__format_message(message)
            elif message.category == 'C':
                nb_conventions += 1
                text_conventions += self.__format_message(message)
            elif message.category == 'R':
                nb_refactors += 1
                text_refactors += self.__format_message(message)

        if nb_errors > 0:
            summary_file.write('## Errors ({})\n{}\n'.format(
                nb_errors, text_errors))

        if nb_warnings > 0:
            summary_file.write('## Warnings ({})\n{}\n'.format(
                nb_warnings, text_warnings))

        if nb_conventions > 0:
            summary_file.write('## Conventions ({})\n{}\n'.format(
                nb_conventions, text_conventions))

        if nb_refactors > 0:
            summary_file.write('## Refactors ({})\n{}\n'.format(
                nb_refactors, text_refactors))

        # Write the scores for each file.
        summary_file.write('# Scores, sorted from worst to best\n')
        total_score = 0

        for entry in self.results.scores:
            summary_file.write('{:.2f} : {}\n'.format(entry.score, entry.filename))
            total_score += entry.score

        # Write some summary.
        summary_file.write(
            '\n# Summary\n'
            'Analyzed folder: {}\n'
            '{} file(s) analyzed\n'
            'Average score is {:.2f}\n'.format(
                self.results.options.dir, nb_files, total_score / nb_files))

    def __format_message(self, message):
        """ Formats and returns the message, for the report. """
        if message.column > 0:
            return '{}, line {}, col. {}: {}\n'.format(
                message.filename, message.line, message.column, message.message)
        else:
            return '{}, line {}: {}\n'.format(
                message.filename, message.line, message.message)

def parse_arguments():
    """ Parses the arguments given on the command line. """
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help='The folder in which to run the check')
    options = parser.parse_args()

    # Compute the paths.
    global REPORT_DIR
    global REPORT_DETAILED_DIR
    global REPORT_SUMMARY_FILENAME
    REPORT_DIR = os.path.join(os.path.abspath(options.dir), '__pylint_dir__report__')
    REPORT_DETAILED_DIR = os.path.join(REPORT_DIR, 'detailed')
    REPORT_SUMMARY_FILENAME = os.path.join(REPORT_DIR, 'summary.txt')

    return options

def validate_pylint_install():
    """
    Makes sure pylint is installed.
    If not, exits the script.
    """
    try:
        subprocess.call(
            [PYLINT_EXEC, '--version'],
            shell=True, stdout=open(os.devnull, 'wb'), stderr=open(os.devnull, 'wb'))
    except OSError:
        print('ERROR: {} not found.'.format(PYLINT_EXEC), file=sys.stderr)
        exit(1)

def empty_folder(dirname):
    """ Removes the content of a folder, without deleting the folder itself. """
    for file_object in os.listdir(dirname):
        file_path = os.path.join(dirname, file_object)

        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as exc:
            print(exc)

def main(_):
    """ Main script entry point. """
    options = parse_arguments()
    validate_pylint_install()

    results = AnalysisResults(options)
    results.analyze_dir()

    report = BasicReport(results)
    report.report()

if __name__ == '__main__':
    main(sys.argv[1:])
