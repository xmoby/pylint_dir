# Introduction
This script will walk a folder recursively and report pylint messages for
all .py files found, in a nice summary.

# Requirements
- Python 2.7+ or 3.x
- pylint

# TODO
Here is a list of possible improvements.

## Soon
- Configurable output folder.
- Configurable pylint executable path.
- Have a min-category option, to hide any message below a threshold.
- Have the possibility to disable details for each file.
- Have the possibility to ignore some specific pylint messages.

## Later, maybe
- Different output format(s).
- Incremental checks, to not rescan a file that did not change.
-- Compare the timestamp between the .py and detailed/.txt files.
-- If the details is more recent, read it instead of running pylint (it's the same output).
-- Allow to force a rescan, with an argument from the command line.
- Sort messages in summary by category, type then filename.
-- Instead of simply category then filename.
- Include the previous scores and average in the summary, with the variation (+ or -).
